@extends('layouts.dash')

@section('content')
<div class="container">
	<div class="col-md-12">
		@if(session('success'))
		<div class="alert alert-success" role="alert">
			{{ session('success') }}
		</div>
		@endif
		@if(session('danger'))
		<div class="alert alert-danger" role="alert">
			{{ session('danger') }}
		</div>
		@endif
		<div class="card">
			<div class="card-header card-header-success">
				<h4 class="card-title">
					{{ $local->empresa }}
				</h4>
				<p class="card-category">Editar dados do local e descontos</p>
			</div>
			<div class="card-body">
				<form action="{{ route('editar_local', $local->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label class="bmd-label-floating">Endereço</label>
								<input type="text" class="form-control" name="endereco" value="{{ $local->endereco }}">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="bmd-label-floating">CEP</label>
								<input type="text" class="form-control cep" name="cep" value="{{ $local->cep }}">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="bmd-label-floating">CPF/CNPJ</label>
								<input type="text" class="form-control" name="cpf_cnpj" value="{{ $local->cpf_cnpj }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<select name="estado" class="form-control">
									@foreach($maps as $estado)
									<option value="{{ $estado->id }}" {{ $estado->id == $local->estado_id ? 'selected' : '' }}>{{ $estado->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<select name="cidade" class="form-control">
									@foreach($maps[0]->City as $cidade)
									<option value="{{ $cidade->id }}" {{ $cidade->id == $local->cidade_id ? 'selected' : '' }}>{{ $cidade->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="bmd-label-floating">Latitude</label>
								<input type="text" class="form-control" name="lat" value="{{ $local->lat }}">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="bmd-label-floating">Longitude</label>
								<input type="text" class="form-control" name="long" value="{{ $local->long }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="bmd-label-floating">E-mail</label>
								<input type="email" class="form-control" name="email" value="{{ $local->email }}">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<select name="categoria_id" class="form-control">
									@foreach($categorias as $cat)
									<option value="{{ $cat->id }}" {{ $cat->id == $local->categoria_id ? 'selected' : '' }}>{{ $cat->nome }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="bmd-label-floating">Contato</label>
								<input type="text" class="form-control phone_ddd" name="contato" value="{{ $local->contato }}">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="bmd-label-floating">Horário de abertura</label>
								<input type="text" class="form-control hour" name="abertura" value="{{ $local->abertura }}">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label class="bmd-label-floating">Horário de fechamento</label>
								<input type="text" class="form-control hour" name="fechamento" value="{{ $local->fechamento }}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label class="bmd-label-floating">Perfil</label>
							<input type="file" class="form-control" name="perfil">
						</div>
						<div class="col-md-6">
							<label class="bmd-label-floating">Local 1</label>
							<input type="file" class="form-control" name="img1">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label class="bmd-label-floating">Local 2</label>
							<input type="file" class="form-control" name="img2">
						</div>
						<div class="col-md-6">
							<label class="bmd-label-floating">Local 3</label>
							<input type="file" class="form-control" name="img3">
						</div>
					</div>
					<br>
					<div class="row">
						@if(count(collect($local->Image)) > 0)
						@foreach($local->Image as $image)
						<div class="col-md-3 col-sm-3">
							<div class="card card-stats">
								<div class="card-header {{ $image->perfil == true ? 'card-header-warning' : 'card-header-success' }}">
									<img src="{{ Storage::url($image->url) }}" width="100%">
								</div>
								<div class="card-footer">
									<div class="pull-right">
										<a href="{{ route('excluir_imagem', ['image_id' => $image->id, 'local_id' => $local->id]) }}" class="btn btn-sm btn-danger">Excluir</a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						@endif
					</div>
					<div class="row">
						<div class="col-md-12">
							<strong>Dias de abertura</strong><br>
							@foreach($dias_abertura as $dias)
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" value="{{ $dias->id }}" name="dias_abertura[]" {{ $local->DiasAbertura->contains('dias_abertura_id', $dias->id) ? 'checked' : '' }}>{{ $dias->dia }}
									<span class="form-check-sign"><span class="check"></span></span>
								</label>
							</div>
							@endforeach
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="form-group">
									<label class="bmd-label-floating">Descrição do local</label>
									<textarea class="form-control" rows="2" name="descricao">{{ $local->descricao }}</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<strong>Ambiente</strong><br>
							@foreach($ambiente as $amb)
							<div class="form-check form-check-inline">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" value="{{ $amb->id }}" name="ambiente[]" {{ $local->Ambiente->contains('ambiente_id', $amb->id) ? 'checked' : '' }}>{{ $amb->descricao }}
									<span class="form-check-sign"><span class="check"></span></span>
								</label>
							</div>
							@endforeach
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<strong>Tags</strong>
							<input name="tags" class="form-control tags" value="@foreach($local->tag as $tag){{ $tag->tag.',' }} @endforeach" />
						</div>
					</div>
					@if(count($local->Desconto))
					<hr />
					<strong>Descontos</strong>
					<div class="row">
						@foreach($local->Desconto as $desconto)
						<div class="col-md-6 col-sm-6">
							<div class="card card-stats">
								<div class="card-header card-header-success card-header-icon">
									<div class="card-icon">
										<i class="material-icons">monetization_on</i>
									</div>
									<p class="card-category">Desconto</p>
									<h4 class="card-title"><small>R$</small>{{ $desconto->desconto }}</h4>
									<p class="card-category">Validade</p>
									<h4 class="card-title">{{ date_br($desconto->validade) }}</h4>
									<p class="card-category">Quantidade</p>
									<h4 class="card-title">{{ $desconto->quantidade }}</h4>
								</div>
								<div class="card-footer">
									<div class="stats">
										<i class="material-icons text-success">textsms</i>
										<h5>
											<p>
												{{ $desconto->descricao }}
											</p>
										</h5>
									</div>
								</div>
								<div class="col-md-12">
									<a href="{{ route('status_desconto', ['desconto_id' => $desconto->id, 'local_id' => $local->id]) }}" class="btn btn-sm {{ $desconto->ativo == 1 ? 'btn-danger' : 'btn-success' }}" name="status">{{ $desconto->ativo == 1 ? 'Desativar' : 'Ativar' }}</a>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					@endif
					<!-- Button trigger modal -->
					<button type="button" class="btn btn-sm btn-info pull-right" data-toggle="modal" data-target="#modal_desconto">Cadastrar Desconto</button>

					<button type="submit" class="btn btn-sm btn-success pull-right">Salvar</button>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal Desconto -->
<div class="modal fade" id="modal_desconto" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Cadastrar Desconto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ route('desconto', $local->id) }}" method="POST" autocomplete="off">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="form-group">
									<label class="bmd-label-floating"> Descrição do desconto</label>
									<textarea class="form-control" rows="2" name="desc_desconto"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>validade do desconto</label>
								<input type="date" class="form-control" name="validade" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Qtd de cupons</label>
								<input type="number" class="form-control" name="qtd_cupons" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>R$ Valor</label>
								<input type="text" class="form-control money" name="desconto">
							</div>
						</div>
					</div>
					<button type="button" class="btn btn-sm btn-default pull-right" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-sm btn-success pull-right">Cadastrar</button>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
	</div
	@endsection
