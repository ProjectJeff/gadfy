<!DOCTYPE html>
<html lang="pt-br">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gadfy</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="css/new-age.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Gadfy</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#download">Download</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#features">Sobre o Gadfy</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contato</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-7 my-auto">
            <div class="header-content mx-auto">
              <h1 class="mb-5">Aumente a visibilidade do seu negócio, aqui você anuncia e quem está próximo o encontra em um click!</h1>
              <a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Quer fazer parte deste time?</a>
            </div>
          </div>
          <div class="col-lg-5 my-auto">
            <div class="device-container">
              <div class="device-mockup iphone6_plus portrait white">
                <div class="device">
                  <div class="screen">
                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                    <img src="img/print1.png" class="img-fluid" alt="">
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="download bg-primary text-center" id="download">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <!--<h2 class="section-heading">Intediado?</h2>
            <p>Encontre os melhores lugares da sua região.</p>-->
						<h2 class="section-heading">Em breve o app estará disponível no Google Play e App Store!</h2>
            <div class="badges">
              <a class="badge-link js-scroll-trigger" href="#contact"><img src="img/google-play-badge.svg" alt=""></a>
              <a class="badge-link js-scroll-trigger" href="#contact"><img src="img/app-store-badge.svg" alt=""></a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="features" id="features">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Um guia completo para quem quer sair da rotina.</h2>
          <p class="text-muted">Com o Gadfy você tem: </p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-4 my-auto">
            <div class="device-container">
              <div class="device-mockup iphone6_plus portrait white">
                <div class="device">
                  <div class="screen">
                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                    <img src="img/print2.png" class="img-fluid" alt="">
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-location-pin text-primary"></i>
                    <h3>Guia</h3>
                    <p class="text-muted">Um guia completo para encontrar os melhores lugares da sua região.</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-present text-primary"></i>
                    <h3>Tickets</h3>
                    <p class="text-muted">Você pode encontrar descontos em nossos locais parceiros.</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-like text-primary"></i>
                    <h3>Avaliação</h3>
                    <p class="text-muted">O Gadfy permite avaliar nossos parceiros para ter uma melhor experiência</p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="icon-question text-primary"></i>
                    <h3>O que queremos?</h3>
                    <p class="text-muted">Facilitar sua vida : )</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="cta">
      <div class="cta-content">
        <div class="container">
          <h2>Mais acessos.<br>Mais clientes.</h2>
          <a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Faça parte!</a>
        </div>
      </div>
      <div class="overlay"></div>
    </section>

    <section class="contact bg-primary" id="contact">
      <div class="container">
        <h3>Preenhca o formulário abaixo para divulgar seu negócio ou dúvidas.</h3>
				<div class="card col-sm-6" style="margin: 0 auto">
					<div class="card-body">
						<form action="/send_email" method="POST">
							{{ csrf_field() }}
							<div class="form-group">
								<input type="text" name="nome" placeholder="Nome (Empresa)" class="form-control" />
							</div>
							<div class="form-group">
								<input type="phone" name="fone" placeholder="Fone" class="form-control" />
							</div>
							<div class="form-group">
								<input type="email" name="email" placeholder="E-mail" class="form-control" />
							</div>
							<div class="form-group">
								<textarea name="assunto" placeholder="Digite quero participar que entraremos em contato ou digite sua dúvia." class="form-control"></textarea>
							</div>
							<button type="submit" class="btn btn-outline btn-xl">ENVIAR</button>
						</form>
					</div>
				</div>
        <ul class="list-inline list-social" style="padding-top: 30px">
          <li class="list-inline-item social-facebook">
            <a href="https://www.facebook.com/Gadfy-568656163553346/?modal=admin_todo_tour" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="list-inline-item social-google-plus">
            <a href="https://www.instagram.com/Gadfy_app/" target="_blank">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
        </ul>
      </div>
    </section>

    <footer>
			<div class="container">
				<p>suporte@gadfy.com.br</p>
        <p>&copy; Gadfy 2018. All Rights Reserved.</p>
			</div>
      <!--<div class="container">
        <p>&copy; Gadfy 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Privacy</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Terms</a>
          </li>
          <li class="list-inline-item">
            <a href="#">FAQ</a>
          </li>
        </ul>
      </div>-->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/new-age.min.js"></script>

  </body>

</html>
