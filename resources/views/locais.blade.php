@extends('layouts.dash')

@section('content')

<div class="col-md-12">
	@if(session('success'))
	<div class="alert alert-success" role="alert">
		{{ session('success') }}
	</div>
	@endif
	@if(session('danger'))
	<div class="alert alert-danger" role="alert">
		{{ session('danger') }}
	</div>
	@endif
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_cad">
		Cadastrar Local
	</button>
	<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_category">
		Cadastrar Categoria
	</button>
	<div class="table-responsive">
		<!-- End Navbar -->
		<div class="card">
			<div class="card-header card-header">
				<h4 class="card-title ">Pesquisar</h4>
				<form action="{{ route('locais_search') }}" method="POST">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-10">
							<input type="text" class="form-control" name="empresa" placeholder="Empresa">
						</div>
						<div class="col-2">
							<button type="submit" class="btn btn-sm btn-success">
								<i class="material-icons">search</i>
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead class="text-success">
							<th>ID</th>
							<th>Empresa</th>
							<th>Endereço</th>
							<th>Cep</th>
							<th class="text-center">Opções</th>
						</thead>
						<tbody>
							@foreach($locais as $local)
							<tr>
								<td>{{ $local->id }}</td>
								<td>{{ $local->empresa }}</td>
								<td>{{ $local->endereco }}</td>
								<td>{{ $local->cep }}</td>
								<td class="text-center">
									<a class="btn btn-sm btn-success" href="{{ route('local', $local->id) }}">Descontos</a>

									<a class="btn btn-sm {{ $local->ativo ? 'btn-danger' : 'btn-success' }}" href="{{ route('local_status', ['id' => $local->id, 'status' => $local->ativo]) }}">{{ $local->ativo ? 'Desativar' : 'Ativar' }}</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $locais->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal CADASTRAR -->
<div class="modal fade" id="modal_cad" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Cadastrar Local</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ route('locais') }}" method="post" autocomplete="off">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="empresa" class="bmd-label-floating">Empresa</label>
								<input type="text" class="form-control" name="empresa" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="cep" class="bmd-label-floating">CEP</label>
								<input type="text" class="form-control cep" name="cep" minlength="8" maxlength="8" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="endereco" class="bmd-label-floating">Endereço</label>
								<input type="text" class="form-control" name="endereco" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="estado" class="bmd-label">Estado</label>
								<select name="estado" class="form-control">
									@foreach($maps as $map)
									<option value="{{ $map->id }}">{{ $map->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cidade" class="bmd-label">Cidade</label>
								<select name="cidade" class="form-control">
									@foreach($maps[0]->City as $cidade)
									<option value="{{ $cidade->id }}">{{ $cidade->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label for="endereco" class="bmd-label-floating">E-mail</label>
								<input type="email" class="form-control" name="email" required style="margin-top: 14.5%">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="categoria" class="bmd-label">Categoria</label>
								<select name="categoria_id" class="form-control">
									@foreach($categorias as $cat)
									<option value="{{ $cat->id }}">{{ $cat->nome }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="contato" class="bmd-label-floating">Fone/WhatsApp</label>
								<input type="text" class="form-control phone_ddd" name="contato" maxlength="13" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="cpf_cnpj" class="bmd-label-floating">CPF/CNPJ</label>
								<input type="text" class="form-control" name="cpf_cnpj" maxlength="14">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="abertura" class="bmd-label-floating">Abertura</label>
								<input type="text" class="form-control hour" name="abertura" maxlength="5" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="fechamento" class="bmd-label-floating">Fechamento</label>
								<input type="text" class="form-control hour" name="fechamento" maxlength="5" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="lat" class="bmd-label-floating">Latitude</label>
								<input type="text" class="form-control" name="lat" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="long" class="bmd-label-floating">Longitude</label>
								<input type="text" class="form-control" name="long" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Local</label>
								<div class="form-group">
									<label class="bmd-label-floating">Descrição do local</label>
									<textarea class="form-control" rows="2" name="descricao" required></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<strong>Tags</strong>
							<input name="tags" class="form-control tags" />
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
						<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal CADASTRAR -->
<div class="modal fade" id="modal_category" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Cadastrar Categoria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{ route('categoria_store') }}" method="post" autocomplete="off">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
							<strong>Categoria</strong>
							<input name="category" class="form-control" placeholder="Nome da categoria" />
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-sm btn-primary">Cadastrar</button>
						<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
@endsection
