<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Gadfy
  </title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Tags input -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css">
  <!-- CSS Files -->
  <link href="{{ url('/assets/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ url('/assets/demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="{{ url('/assets/img/sidebar-3.jpg') }}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
      -->
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          GADFY
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="{{ route('locais') }}">
              <i class="material-icons">local_offer</i>
              <p>Locais</p>
            </a>
          </li>
          <li class="nav-item active  ">
            <a class="nav-link" href="{{ route('logout') }}">
              <i class="material-icons">exit_to_app</i>
              <p>Sair</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <!--<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
      </nav>
      <!-- End Navbar -->
      <br>
      <div class="container-fluid">
        @yield('content')
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>
            Gadfy
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="{{ url('/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('/assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="{{ url('/assets/js/plugins/chartist.min.js') }}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{ url('/assets/js/plugins/bootstrap-notify.js') }}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ url('/assets/js/material-dashboard.min.js?v=2.1.0') }}" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{ url('/assets/demo/demo.js') }}"></script>
  <!--Mask for input -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
  <!-- Tags input -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"></script>

  <!-- TAGS INPUT -->
  <script type="text/javascript">
    $(document).ready(function(){
      $('.tags').tagsInput({
        'width':'100%',
        'height':'60px',
        'defaultText':'+ Tags'
      });

      /*$('.tags').tagsInput({
       'autocomplete_url': url_to_autocomplete_api,
       'autocomplete': { option: value, option: value},
       'height':'100px',
       'width':'300px',
       'interactive':true,
       'defaultText':'add a tag',
       'onAddTag':callback_function,
       'onRemoveTag':callback_function,
       'onChange' : callback_function,
       'delimiter': [',',';'],   // Or a string with a single delimiter. Ex: ';'
       'removeWithBackspace' : true,
       'minChars' : 0,
       'maxChars' : 0, // if not provided there is no limit
       'placeholderColor' : '#666666'
      });*/
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.date').mask('00/00/0000', {placeholder: "__/__/____"});
      $('.time').mask('00:00:00');
      $('.hour').mask('00:00');
      $('.date_time').mask('00/00/0000 00:00:00');
      $('.cep').mask('00.000-000');
      $('.phone').mask('0000-0000');
      $('.phone_ddd').mask('(00)00000-0000');
      $('.mixed').mask('AAA 000-S0S');
      $('.cpf').mask('000.000.000-00', {reverse: true});
      $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
      $('.money').mask('000.000.000.000.000,00', {reverse: true});
      $('.money2').mask("#.##0,00", {reverse: true});
      $('.ip_address').mask('099.099.099.099');
      $('.percent').mask('##0,00%', {reverse: true});
      $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
      $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
    });
  </script>
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
</body>

</html>
