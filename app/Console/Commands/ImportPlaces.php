<?php

namespace App\Console\Commands;

use App\Image;
use Illuminate\Console\Command;
use GuzzleHttp\Client as client;
use Illuminate\Database\Eloquent\Collection;
use App\State;
use App\City;
use App\Categoria;
use App\Local;

class ImportPlaces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:places';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Places Here api';

    private $uri = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
    private $uri_detail = 'https://maps.googleapis.com/maps/api/place/details/json';
    private $key = 'AIzaSyDLaapEiapp4inlQUmzalv-0CNUMPYpas4';
    private $client;
    private $position = '-30.03752882,-51.22631642';
    public $categories = [
        'jewelry_store', 'library', 'bar', 'book_store', 'bowling_alley', 'movie_theater',
        'night_club', 'pet_store', 'pharmacy', 'clothing_store', 'restaurant', 'dentist', 'gym'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->output->progressStart(count($this->categories));
            foreach ($this->categories as $category) {
                $places = $this->client->get($this->uri . '?location=' . $this->position . '&radius=25000&type=' . $category . '&key=' . $this->key);
                $places = $places->getBody()->getContents();
                $places = json_decode($places);
                //$next_page = !empty($places->next_page_token) ? $places->next_page_token : null;

                foreach ($places->results as $place) {

                    $p = $this->client->get($this->uri_detail . '?placeid='.$place->reference.'&key=' . $this->key);
                    $p = $p->getBody()->getContents();
                    $p = json_decode($p);

                    $local = Local::firstOrCreate($this->parse($p->result, $category));

                    if(!empty($place->photos)){
                        foreach ($place->photos as $picture){
                            $profile_exist = Image::where('local_id', '=', $local->id)->get();
                            $photo = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference='.$picture->photo_reference.'&key=';
                            $profile = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference='.$picture->photo_reference.'&key=';
                            Image::firstOrCreate([
                                'url'           => $photo,
                                'local_id'      => $local->id,
                                'user_gadfy_id' => '',
                                'perfil'        => !empty($profile_exist->perfil) ? null : $profile
                            ]);
                        }
                    }
                }
                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
        } catch (\Exception $e) {
            dump($e);
        }
    }


    public function parse($data, $category)
    {
        return [
            'empresa'       => $data->name,
            'endereco'      => $data->vicinity,
            'cep'           => null,
            'estado_id'     => null,
            'cidade_id'     => null,
            'rating'        => !empty($data->rating) ? $data->rating : '4.0',
            'site'          => @$data->website,
            'email'         => null,
            'contato'       => @$data->formatted_phone_number,
            'cpf_cnpj'      => null,
            'abertura'      => null,
            'fechamento'    => null,
            'categoria_id'  => $this->parseCategory($category)->id,
            'lat'           => $data->geometry->location->lat,
            'lng'           => $data->geometry->location->lng,
            'descricao'     => null
        ];
    }

    public function parseState($state)
    {
        $state_id = State::where('name', 'LIKE', $state)->first();
        return $state_id ? $state_id : null;
    }

    public function parseCity($city)
    {
        $city_id = City::where('name', 'LIKE', $city)->first();
        return $city_id ? $city_id : null;
    }

    public function parseCategory($category)
    {
        try {
            $category = Categoria::firstOrCreate(['nome' => $category]);
            return $category;
        } catch (\Exception $e) {
            return null;
        }
    }

}
