<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigo extends Model
{
    protected $fillable = ['codigo', 'desconto_id', 'user_gadfy_id', 'resgate'];
}
