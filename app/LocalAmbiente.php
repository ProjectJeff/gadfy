<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalAmbiente extends Model
{
	protected $table = 'local_ambiente';
    protected $fillable = ['local_id', 'ambiente_id'];
}
