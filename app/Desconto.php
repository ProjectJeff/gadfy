<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desconto extends Model
{
    protected $fillable = [
    	'local_id',
    	'descricao',
    	'desconto',
    	'quantidade',
    	'validade',
    	'ativo'
    ];

    public function Local()
     {
        return $this->hasOne('App\Local');
     }
}
