<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Local;
use App\DiasAbertura;
use App\Ambiente;
use App\Desconto;
use App\Categoria;

use App\Repositories\LocalRepository;
use App\Repositories\ImageRepository;

class LocaisController extends Controller
{
    public function index(Request $request)
    {
      $empresa = $request->empresa;
    	$locais = Local::OrderBy('created_at', 'desc')
        ->when($empresa, function($q) use($empresa){
            $q->where('empresa', 'LIKE', '%'.$empresa.'%');
        })->paginate(10);
    	$maps = State::with('City')->where('id', '=', 43)->get();
      $categorias = Categoria::OrderBy('nome', 'asc')->get();
      //dd($categorias);
    	return view('locais', compact('maps', 'locais', 'categorias'));
    }

    public function store(Request $request, LocalRepository $local)
    {
        $local->storeLocal($request->all());
        return redirect('/locais');
    }

    public function edit($id)
    {
        $local = Local::with(['Desconto', 'DiasAbertura', 'Ambiente', 'Tag', 'Image'])
        ->where('id', '=', $id)
        ->first();
        $maps = State::with('City')->where('id', '=', 43)->get();
        $dias_abertura = DiasAbertura::all();
        $ambiente = Ambiente::all();
        $categorias = Categoria::OrderBy('nome', 'asc')->get();

        return view('locais_edit', compact('local', 'maps', 'dias_abertura', 'ambiente', 'categorias'));

    }

    public function editLocal($id, Request $request, ImageRepository $image, LocalRepository $local)
    {
        $image->storeImage($request->perfil, $request->img1, $request->img2, $request->img3, $id);
        $local->editLocal($id, $request->all());
        return redirect('/locais/'.$id);
    }

    public function status($id, $status)
    {
        try{
            Local::find($id)->update([
                'ativo' => !$status
            ]);
            session()->flash('success', 'Status alterado.');
            return redirect('/locais');
        }catch(\Exception $e){
            dd($e->getMessage());
            session()->flash('danger', 'Ocorreu um erro ao atualizar status.');
            return redirect('/locais');
        }
    }
}
