<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\DescontoRepository;

class DescontoController extends Controller
{
	public function store($id, Request $request, DescontoRepository $desconto)
	{
		$desconto->storeDesconto($id, $request->all());

		return redirect('/locais/'.$id);
	}

	public function statusDesconto($desconto_id, $local_id, DescontoRepository $desconto)
	{
		$desconto->statusDesconto($desconto_id);

		return redirect('/locais/'.$local_id);
	}
}
