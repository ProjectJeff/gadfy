<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendEmail(Request $request) {

        Mail::to('suporte@gadfy.com.br')
            ->send(new ContactMail($request->all()));

        return redirect()->back();
    }
}
