<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function store(Request $request)
    {
      try{
        Categoria::firstOrCreate(['nome'  => $request->category]);
        session()->flash('success', 'Categoria cadastrada com sucesso.');
        return redirect('/locais');
  		} catch (\Exception $e) {
  			session()->flash('danger', 'Houve um erro ao cadastrar categoria.');
        return redirect('/locais');
  		}
    }
}
