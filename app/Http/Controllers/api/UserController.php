<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserGadfy;

class UserController extends Controller
{
  public function store(Request $request)
  {
    try{
      $user = UserGadfy::firstOrCreate([
        'nome'        => $request->name,
        'email'       => $request->email,
        'fone'        => $request->fone,
        'senha'       => $request->senha,
        'picture'     => $request->picture,
        'login_fb'    => $request->login_fb,
        'login_insta' => $request->login_insta
      ]);
      return response()->json(['message' => 'success', 'user' => $user], 200);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }


  public function getUser(Request $request)
  {
    try{
      $user = UserGadfy::where('email', '=', $request->email)
                       ->where('senha', '=', $request->senha)->get();
      if($user){
        return response()->json(['message' => 'success', 'user' => $user], 200);
      }
      return response()->json(['message' => 'error', 'user' => null], 401);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }
}
