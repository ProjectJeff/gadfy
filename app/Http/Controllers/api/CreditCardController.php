<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CreditCard;

class CreditCardController extends Controller
{
  public function store(Request $request){
    try{
      CreditCard::firstOrCreate([
        'user_gadfy_id'   => $request->user_gadfy_id,
        'cvc'             => $request->cvc,
        'number'          => $request->number,
        'expirationMonth' => $request->expirationMonth,
        'expirationYear'  => $request->expirationYear,
        'cpf'             => $request->cpf
      ]);
      return response()->json(['message' => 'success'], 200);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }


  public function remove(Request $request)
  {
    try{
      CreditCard::where('user_gadfy_id', '=', $request->user_gadfy_id)->delete();
      return response()->json(['message' => 'success'], 200);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }

  public function get(Request $request)
  {
    try{
      $card = CreditCard::where('user_gadfy_id', '=', $request->user_gadfy_id)->get();
      return response()->json(['message' => 'success', 'creditCard' => $card], 200);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }
}
