<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plano;

class PlanosController extends Controller
{
  public function getAll()
  {
    try{
      $plans = Plano::all();
      return response()->json(['message' => 'success', 'plans' => $plans], 200);
    }catch(\Exception $e){
      return response()->json(['message' => $e->getMessage()], 400);
    }
  }
}
