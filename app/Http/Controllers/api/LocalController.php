<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Local;
use GuzzleHttp\Client as client;
use Illuminate\Database\Eloquent\Collection;

class LocalController extends Controller
{
    private $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new client();
    }

    public function address(Request $request)
    {
        try {
            $sql = $request->category_id != "null" ? 'where categoria_id = ' . $request->category_id : 'where 1 = 1';

            $response = collect(DB::select(DB::raw("select locais.id,
            locais.empresa, locais.endereco, locais.email, locais.contato, locais.abertura,
            locais.fechamento, locais.descricao, locais.ativo,
            cities.name as cidade, states.name as estado, images.perfil,
            (6371 * acos( cos( radians(".$request->lat.") )
            * cos( radians( lat ) )
            * cos( radians( lng ) - radians(".$request->lng.") )
            + sin( radians(".$request->lat.") )
            * sin( radians( lat ) ) ) ) AS distancia
            from locais
            left join cities on (locais.cidade_id = cities.id)
            left join states on (cities.state_id = states.id)
            left join images on (images.local_id = locais.id)
            " . $sql . "
            HAVING distancia < 15.000
            ORDER BY distancia
            ")));

            $response = $response->forPage($request->page, 12);

            return response()->json(['message' => 'success', $response], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }


    public function getPlace(Request $request)
    {
        try{
            $place = Local::where('id', '=', $request->place_id)
                ->with(['State', 'Citie', 'Ambiente', 'Image', 'Tag', 'DiasAbertura', 'Desconto'])
                ->orderBy('created_at', 'desc')->get();

            return response()->json(['message' => 'success', 'place' => $place], 200);
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function calcCoordenate($data, $lat, $lng)
    {
        $myposition = $this->latLngToAddress($lat, $lng);
        try {
            foreach ($data as $address) {
                $adds = $address->endereco . ' ' . $address->Citie[0]->name . ' ' . @$address->State[0]->name;
                $distance = $this->calculeDistance($myposition, $adds);
                $distance = $distance['rows'][0]['elements'][0]['distance']['value'];

                $data = $data->filter(function ($local) use ($distance, $address) {
                    ($address->id == $local->id) ? $local['distancia'] = $distance : null;
                    return $local;
                });
            }

            return $data;
        } catch (\Exception $e) {
            return [];
        }
    }

    //TRANSFORMA A LATITUDE E lngITUDE EM ENDEREÇO
    public function latLngToAddress($lat, $lng)
    {
        try {
            $response = $this->client->get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lng . '&key=AIzaSyDLaapEiapp4inlQUmzalv-0CNUMPYpas4');
            $response = $response->getBody()->getContents();
            $response = json_decode($response);

            $local = $response->results;

            return $local[0]->formatted_address;
        } catch (\Exception $e) {
            return [];
        }
    }

    //CALCULA A DISTANCIA ENTE ENDEREÇOS
    public function calculeDistance($address, $finalAddres)
    {
        try {
            $response = $this->client->get('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' . $address . '&destinations=' . $finalAddres . '&mode=driving&language=pt-BR&sensor=false&key=AIzaSyDLaapEiapp4inlQUmzalv-0CNUMPYpas4');
            $response = $response->getBody()->getContents();
            $response = json_decode($response, true);

            return $response;
        } catch (\Exception $e) {
            return [];
        }
    }
}
