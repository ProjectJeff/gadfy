<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categoria;

class CategoryController extends Controller
{
    public function get()
    {
      try{
        $categories = Categoria::all();
        return response()->json(['message' => 'success', 'categories' => $categories], 200);
      }catch(\Exception $e) {
        return response()->json(['message' => 'error', []], 400);
      }
    }
}
