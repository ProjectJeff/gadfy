<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Codigo;
use App\Ticket;

class TicketController extends Controller
{
    public function get(Request $request)
    {

      try{
        $total_codigos = Codigo::where('user_gadfy_id', '=', $request->user_gadfy_id)->count();
        $total_tickets = Ticket::where('user_gadfy_id', '=', $request->user_gadfy_id)->sum('qtd_tickets');

        $total = ($total_tickets - $total_codigos);

        return response()->json(['message' => 'success', 'tickets' => $total], 200);
      }catch(\Exception $e){
        return response()->json(['message' => $e->getMessage()], 400);
      }


    }


    public function store(Request $request)
    {
      try{
        Ticket::create([
          'user_gadfy_id'   => $request->user_gadfy_id,
          'plano_id'        => $request->plan,
          'qtd_tickets'     => $request->qtd_tickets,
          'moip_pedido'     => $request->moip_pedido,
          'moip_pagamento'  => $request->moip_pagamento
        ]);

        return response()->json(['message' => 'success'], 200);
      }catch(\Exception $e){
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }


    public function storeCodigo(Request $request)
    {
      try {
        $codigo = strtoupper(substr(md5($request->desconto_id.$request->user_gadfy_id.uniqid()), 0, 7));
        Codigo::create([
          'codigo'        => $codigo,
          'desconto_id'   => $request->desconto_id,
          'user_gadfy_id' => $request->user_gadfy_id
        ]);
        return response()->json(['message' => 'success', 'codigo' => $codigo], 200);
      }catch(\Exception $e){
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }



    public function getCodigo(Request $request)
    {
      try{
        $codigos = Codigo::where('user_gadfy_id', '=', $request->user_gadfy_id)
                          ->orderBy('created_at', 'DESC')
                          ->get();
        return response()->json(['message' => 'success', 'codigo' => $codigos], 200);
      }catch(\Exception $e){
        return response()->json(['message' => $e->getMessage()], 400);
      }
    }
}
