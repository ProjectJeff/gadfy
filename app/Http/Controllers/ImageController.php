<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{
	public function destroy($id_image, $id_local)
	{
		try{
			Image::find($id_image)->delete();
			session()->flash('success', 'Imagem removida.');
			return redirect('/locais/'.$id_local);
		}catch(\Exception $e){
			session()->flash('danger', 'Erro ao remover imagem.');
			return redirect('/locais/'.$id_local);
		}
	}
}
