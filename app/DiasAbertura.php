<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiasAbertura extends Model
{
    protected $table = 'dias_abertura';

    protected $fillable = ['id', 'dia'];
}
