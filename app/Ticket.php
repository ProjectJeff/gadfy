<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['user_gadfy_id', 'plano_id', 'qtd_tickets', 'moip_pedido', 'moip_pagamento'];
}
