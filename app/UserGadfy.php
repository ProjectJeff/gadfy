<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGadfy extends Model
{
    protected $table = 'user_gadfy';

    protected $fillable = ['nome', 'email', 'fone', 'senha', 'picture', 'login_fb', 'login_insta'];
}
