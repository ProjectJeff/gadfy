<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    protected $fillable = ['user_ged_id', 'cvc', 'number', 'expirationMonth', 'expirationYear', 'cpf'];
}
