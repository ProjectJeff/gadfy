<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalDiasAbertura extends Model
{
	protected $table = 'local_dias_abertura';
    protected $fillable = ['local_id', 'dias_abertura_id'];
}
