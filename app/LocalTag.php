<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocalTag extends Model
{
    protected $fillable = ['local_id', 'tag_id'];
}
