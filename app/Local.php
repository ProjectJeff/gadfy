<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
	protected $table = 'locais';

    protected $fillable =
    ['empresa',
     'endereco',
     'cep',
     'estado_id',
     'cidade_id',
     'email',
     'contato',
     'cpf_cnpj',
     'abertura',
     'fechamento',
     'descricao',
		 'categoria_id',
     'lat',
     'lng',
     'ativo'];

		 public function State()
     {
         return $this->hasMany('App\State', 'id', 'estado_id');
     }

     public function Citie()
     {
         return $this->hasMany('App\City', 'id', 'cidade_id');
     }

     public function Desconto()
     {
        return $this->hasMany('App\Desconto', 'local_id', 'id');
     }

     public function DiasAbertura()
     {
        return $this->hasMany('App\LocalDiasAbertura', 'local_id', 'id');
     }

     public function Ambiente()
     {
        return $this->hasMany('App\LocalAmbiente', 'local_id', 'id');
     }

     public function Image()
     {
        return $this->hasMany('App\Image', 'local_id', 'id');
     }

     public function Tag()
    {
        return $this->belongsToMany('App\Tag', 'local_tags', 'local_id');
    }
}
