<?php

if (! function_exists('transform_real')) {
	function transform_real($number)
	{
		$real = number_format($number, 2, ',', '.');
		return $real;
	}
}

if (! function_exists('date_atual_br')) {
	function date_atual_br()
	{
		date_default_timezone_set('America/Sao_Paulo');

		return date('d/m/Y');
	}
}


if (! function_exists('date_br')) {
	function date_br($date)
	{
		return date('d/m/Y', strtotime($date));
	}
}


if(! function_exists('limpa_variavel')) {
	function limpa_variavel($var)
	{
		$var = trim($var);
		$var = str_replace(".", "", $var);
		$var = str_replace(",", "", $var);
		$var = str_replace("-", "", $var);
		$var = str_replace("/", "", $var);
		$var = str_replace("(", "", $var);
		$var = str_replace(")", "", $var);
		$var = str_replace(" ", "", $var);
		return $var;
	}
}
