<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

if (! function_exists('store_image')) {
	function store_image($image)
	{
		$image = Storage::disk('local')->put('public/images', $image, 'public');
		return $image;
	}
}
