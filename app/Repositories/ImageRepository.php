<?php

namespace App\Repositories;

use App\Image;

class ImageRepository
{
	private $image;

	public function __construct(Image $image)
	{
		$this->image = $image;
	}

	public function storeImage($perfil=null, $img1=null, $img2=null, $img3=null, $id)
	{
		$img_perfil = $this->image->where('local_id', '=', $id)
		->where('perfil', '=', true)
		->count();

		try{
			if($perfil && $img_perfil == 0)
			{
				$perfil = store_image($perfil);
				$this->image->create(['url' => $perfil, 'local_id' => $id, 'perfil' => true]);
			}

			$img = $this->image->where('local_id', '=', $id)->count();
			if($img < 4){
				if($img1)
				{
					$img1 = store_image($img1);
					$this->image->create(['url' => $img1,'local_id' => $id]);
				}
				if($img2)
				{
					$img2 = store_image($img2);
					$this->image->create(['url' => $img2,'local_id' => $id]);
				}
				if($img1)
				{
					$img3 = store_image($img3);
					$this->image->create(['url' => $img3,'local_id' => $id]);
				}
			}
			return true;
		}catch(\Exception $e){
			dd($e->getMessage());
		}
	}
}
