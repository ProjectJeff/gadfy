<?php

namespace App\Repositories;

use App\Local;
use App\LocalTag;
use App\Tag;
use App\LocalAmbiente;
use App\LocalDiasAbertura;

class LocalRepository
{
	private $local;
	private $tag;
	private $local_tag;
	private $local_ambiente;
	private $local_dias;

	public function __construct(Local $local, Tag $tag, LocalTag $local_tag, LocalAmbiente $local_ambiente, LocalDiasAbertura $local_dias)
	{
		$this->local 			= $local;
		$this->tag 				= $tag;
		$this->local_tag 		= $local_tag;
		$this->local_ambiente 	= $local_ambiente;
		$this->local_dias 		= $local_dias;

	}

	public function storeLocal($data)
	{
		$tags = explode(',', $data['tags']);

		try {
			$local_id = $this->local->firstOrCreate([
				'empresa' 		=> $data['empresa'],
				'endereco' 		=> $data['endereco'],
				'cep' 			=> limpa_variavel($data['cep']),
				'estado_id' 	=> $data['estado'],
				'cidade_id' 	=> $data['cidade'],
				'email'         => $data['email'],
				'contato'       => limpa_variavel($data['contato']),
				'cpf_cnpj'		=> $data['cpf_cnpj'],
				'abertura'      => $data['abertura'],
				'fechamento'    => $data['fechamento'],
				'categoria_id'	=> $data['categoria_id'],
				'lat'			=> $data['lat'],
				'lng'			=> $data['lng'],
				'descricao'     => $data['descricao']
			]);

          	//CRIANDO TAGS
			for ($i = 0; count(collect($tags)) > $i; $i++) {
				$tag = $this->tag->firstOrCreate(['tag' => $tags[$i]]);

				$this->local_tag->create([
					'local_id'  => $local_id->id,
					'tag_id'    => $tag->id
				]);
			}

			return session()->flash('success', 'Local cadastrado com sucesso.');
		} catch (\Exception $e) {
			return session()->flash('danger', 'Houve um erro ao cadastrar Local.');
		}
	}

	public function editLocal($id, $data)
	{
		$tags = explode(',', $data['tags']);

		try{
			$this->local->find($id)->update([
				'endereco'      => $data['endereco'],
				'cep'           => limpa_variavel($data['cep']),
				'estado_id'     => $data['estado'],
				'cidade_id'     => $data['cidade'],
				'email'         => $data['email'],
				'contato'       => limpa_variavel($data['contato']),
				'cpf_cnpj'		=> $data['cpf_cnpj'],
				'abertura'      => $data['abertura'],
				'fechamento'    => $data['fechamento'],
				'categoria_id'	=> $data['categoria_id'],
				'lat'			=> $data['lat'],
				'lng'			=> $data['lng'],
				'descricao'     => $data['descricao'],
				'ativo'         => true
			]);
            //CRIANDO RELAÇÃO DE DIAS DE ABERTURA
			$this->local_dias->where('local_id', '=', $id)->delete();
			if(!empty($data['dias_abertura'])){
				for($i = 0; count(collect($data['dias_abertura'])) > $i; $i++){
					$this->local_dias->Create([
						'local_id'          => $id,
						'dias_abertura_id'  => $data['dias_abertura'][$i]
					]);
				}
			}
            //CRIANDO RELAÇÃO DE AMBIENTE
			$this->local_ambiente->where('local_id', '=', $id)->delete();
			if(!empty($data['ambiente'])){
				for($i = 0; count(collect($data['ambiente'])) > $i; $i++){
					$this->local_ambiente->Create([
						'local_id'      => $id,
						'ambiente_id'   => $data['ambiente'][$i]
					]);
				}
			}
            //CRIANDO TAGS
			$this->local_tag->where('local_id', '=', $id)->delete();
			for ($i = 0; count(collect($tags)) > $i; $i++) {
				$tag = $this->tag->firstOrCreate(['tag' => $tags[$i]]);
				$this->local_tag->create([
					'local_id'   => $id,
					'tag_id'     => $tag->id
				]);
			}

			return session()->flash('success', 'Editado com sucesso.');
		}catch(\Exception $e){
			dd($e->getMessage());
			return session()->flash('danger', 'Houve um erro ao salvar.');
		}
	}
}
