<?php

namespace App\Repositories;

use App\Desconto;

class DescontoRepository 
{
	private $desconto;

	public function __construct(Desconto $desconto)
	{
		$this->desconto = $desconto;
	}

	public function storeDesconto($id, $data)
	{
		try{
            //SE DESCONTO NÃO EXISTIA CRIA UM NOVO
			$this->desconto->create([
				'local_id'      => $id,
				'descricao'     => $data['desc_desconto'],
				'desconto'      => ($data['desconto']==null ? 0.00 : str_replace(",", ".", $data['desconto'])),
				'quantidade'    => $data['qtd_cupons'],
				'validade'      => $data['validade'],
				'ativo'         => true
			]);
			
			return session()->flash('success', 'Desconto criado com sucesso.');
		}catch(\Exception $e){
			return session()->flash('danger', 'Houve um erro ao criar desconto.');
		}
	}

	public function statusDesconto($desconto_id)
	{
		try{
			$desconto = $this->desconto->find($desconto_id);
			$desconto->update(['ativo' => !$desconto->ativo]);
			return session()->flash('success', 'Desconto atualizado com sucesso.');
		}catch(\Exception $e){
			return rsession()->flash('success', 'Houve um erro ao atualizar desconto.');
		}
	}

}