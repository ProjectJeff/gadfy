<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#->where('lat', '.+');

$router->get('/address', 'api\\LocalController@address');

$router->get('/place/{place_id}', 'api\\LocalController@getPlace');

$router->post('/user/add', 'api\\UserController@store');
$router->get('/user', 'api\\UserController@getUser');

$router->post('/credit_card/add', 'api\\CreditCardController@store');
$router->delete('/credit_card/remove', 'api\\CreditCardController@remove');
$router->get('/credit_card', 'api\\CreditCardController@get');

$router->get('/plans', 'api\\PlanosController@getAll');

$router->get('/tickets/{user_gadfy_id}', 'api\\TicketController@get');
$router->post('/tickets/buy', 'api\\TicketController@store');
$router->post('/tickets/generate_code', 'api\\TicketController@storeCodigo');
$router->get('/tickets/code', 'api\\TicketController@getCodigo');

$router->get('/categories', 'api\\CategoryController@get');
