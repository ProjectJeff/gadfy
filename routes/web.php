<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::post('/send_email', 'EmailController@sendEmail');

Auth::routes();
Route::get('logout', 'HomeController@logout')->name('logout');


Route::group(["middleware" => ["auth"]], function ($r) {

$r->get('/locais', 'LocaisController@index')->name('locais');
$r->post('/locais/search', 'LocaisController@index')->name('locais_search');
$r->post('/locais', 'LocaisController@store')->name('cadastrar_local');
$r->get('/locais/{id}', 'LocaisController@edit')->name('local');
$r->post('/locais/edit/{id}', 'LocaisController@editLocal')->name('editar_local');
$r->get('/locais/status/{id}/{status}', 'LocaisController@status')->name('local_status');

$r->post('/desconto/create/{id}', 'DescontoController@store')->name('desconto');
$r->get('/desconto/status/{desconto_id}/{local_id}', 'DescontoController@statusDesconto')->name('status_desconto');

$r->get('/image/remove/{image_id}/{local_id}', 'ImageController@destroy')->name('excluir_imagem');

$r->get('/planos', 'PlanosController@index')->name('planos');

$r->get('/home', 'HomeController@index')->name('home');

$r->post('/categoria', 'CategoriaController@store')->name('categoria_store');

});
