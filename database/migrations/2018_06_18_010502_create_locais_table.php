<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa', 100);
            $table->string('endereco', 191);
            $table->string('cep', 9)->nullable();
            $table->integer('estado_id')->nullable();
            $table->integer('cidade_id')->nullable();
            $table->string('rating')->nullable();
            $table->string('site')->nullable();
            $table->string('email', 120)->nullable();
            $table->string('contato', 17)->nullable();
            $table->string('cpf_cnpj', 14)->nullable();
            $table->string('abertura', 5)->nullable();
            $table->string('fechamento', 5)->nullable();
            $table->text('descricao')->nullable();
            $table->integer('categoria_id')->nullable();
            $table->float('lat', 10, 6)->nullable();
            $table->float('lng', 10, 6)->nullable();
            $table->boolean('ativo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locais');
    }
}
