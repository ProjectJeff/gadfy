<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGadfyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_gadfy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 191);
            $table->string('email', 88);
            $table->string('fone', 13);
            $table->string('senha', 60);
            $table->string('picture')->nullable();
            $table->boolean('login_fb')->default(0);
            $table->boolean('login_insta')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_gadfy');
    }
}
