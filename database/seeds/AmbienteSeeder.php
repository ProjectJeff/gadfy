<?php

use Illuminate\Database\Seeder;

class AmbienteSeeder extends Seeder
{

	private 
	$ambiente = [
		'Ar-condicionado',
		'Video-game',
		'Churrasqueira',
		'18+',
		'Estacionamento',
		'Wi-fi'
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; count($this->ambiente) > $i; $i++){
    		DB::table('ambiente')->insert([
    			'descricao' => $this->ambiente[$i]
    		]);
    	}
    }
}
