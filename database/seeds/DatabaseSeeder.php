<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(DiasAberturaSeeder::class);
        $this->call(AmbienteSeeder::class);
        $this->call(StatesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(PlanoSeeder::class);
        //$this->call(CategoriaSeeder::class);
        //$this->call(LocaisSeeder::class);
    }
}
