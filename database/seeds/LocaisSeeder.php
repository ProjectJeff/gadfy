<?php

use Illuminate\Database\Seeder;

class LocaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locais')->insert([
    		'empresa' 		=> 'Naski',
    		'endereco' 		=> 'Rua 24 de agosto 356',
    		'cep' 			=> '99999999',
			'estado_id' 	=> 43,
			'cidade_id' 	=> 4307708,
			'email' 		=> 'naski@gmail.com',
			'contato' 		=> '51999999999',
			'cpf_cnpj' 		=> '123223422',
			'abertura' 		=> '10:00',
			'fechamento' 	=> '19:00',
      'categoria_id'  => 7,
			'lat' 			=> '12.7771',
			'long' 			=> '15.76567',
			'descricao' 	=> 'Barbearia do naski'
    	]);

        DB::table('locais')->insert([
            'empresa'       => 'redemac',
            'endereco'      => 'R. 24 de Agosto 2079  Liberdade',
            'cep'           => '93280-001',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'redemac@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 2,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Planeta Xis',
            'endereco'      => 'Rua Pelotas 1144 Centro',
            'cep'           => '93265-000',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'planetaxis@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 6,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'PLAEMAC',
            'endereco'      => 'Av. Benjamin Constant 1024  Conj. 3  São João',
            'cep'           => '90550-000',
            'estado_id'     => 43,
            'cidade_id'     => 4314902,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Acessoria contábil'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Xis da 15',
            'endereco'      => 'Av. Padre Claret 983 Centro',
            'cep'           => '93280-260',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 6,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Master Express',
            'endereco'      => 'Rua Dom Pedro II 37 São João',
            'cep'           => '90550-142',
            'estado_id'     => 43,
            'cidade_id'     => 4314902,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 2,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Estacionamento SERPOL',
            'endereco'      => 'Rua Canoas 68 São jorge',
            'cep'           => '93212-140',
            'estado_id'     => 43,
            'cidade_id'     => 4320008,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 2,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Madeireira Canelense',
            'endereco'      => 'Av. Padre Claret 4635 Walderez',
            'cep'           => '93280-261',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 3,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Barbearia Land bier',
            'endereco'      => 'R. Sen. Salgado Filho 1961 Parque Amador',
            'cep'           => '93260-141',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Naski',
            'endereco'      => 'Rua 24 de agosto 356',
            'cep'           => '99999999',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'brooklin',
            'endereco'      => 'R. 24 de Agosto 2349 Liberdade',
            'cep'           => '93280-001',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Life Fitness',
            'endereco'      => 'Rua Vinte e Quatro de Agosto 1056 Centro',
            'cep'           => '93280-000',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Nova Protector Academia ltda',
            'endereco'      => ' Av. Padre Claret, 1272 - Centro',
            'cep'           => '93281-261',
            'estado_id'     => 43,
            'cidade_id'     => 4307708,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'McDonalds',
            'endereco'      => 'Av. João Corrêa 1098 Centro',
            'cep'           => '93020-690',
            'estado_id'     => 43,
            'cidade_id'     => 4318705,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Bourbon Shopping',
            'endereco'      => 'R. Primeiro de Março 821 Centro',
            'cep'           => '93010-210',
            'estado_id'     => 43,
            'cidade_id'     => 4318705,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
        DB::table('locais')->insert([
            'empresa'       => 'Classic Center',
            'endereco'      => 'R. Cap. Camboim 191 Centro',
            'cep'           => '93218-060',
            'estado_id'     => 43,
            'cidade_id'     => 4320008,
            'email'         => 'naski@gmail.com',
            'contato'       => '51999999999',
            'cpf_cnpj'      => '123223422',
            'abertura'      => '10:00',
            'fechamento'    => '19:00',
            'categoria_id'  => 1,
            'lat'           => '12.7771',
            'long'          => '15.76567',
            'descricao'     => 'Barbearia do naski'
        ]);
    }
}
