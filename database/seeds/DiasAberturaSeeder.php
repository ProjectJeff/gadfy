<?php

use Illuminate\Database\Seeder;

class DiasAberturaSeeder extends Seeder
{
	private 
	$dias = [
		'segunda-feira',
		'terça-feira',
		'quarta-feira',
		'quinta-feira',
		'sexta-feira', 
		'sábado', 
		'domingo', 
		'feriado'
	];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 0; count($this->dias) > $i; $i++){
    		DB::table('dias_abertura')->insert([
    			'dia' => $this->dias[$i]
    		]);
    	}
    }
}
