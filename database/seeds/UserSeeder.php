<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                'name'          => 'jefferson',
    			'email' 		=> 'jeffersondasilva1@hotmail.com',
    			'password'		=> bcrypt('google157'),
    			'api_token'		=> md5('google157')
    	]);
        DB::table('users')->insert([
                'name'          => 'Jackson',
                'email'         => 'souzajackson.ti@gmail.com',
                'password'      => bcrypt('1234pulinho'),
                'api_token'     => md5('1234pulinho')
        ]);
        DB::table('users')->insert([
                'name'          => 'Leo',
                'email'         => 'leosilva163@hotmail.com',
                'password'      => bcrypt('1998101215'),
                'api_token'     => md5('1998101215')
        ]);
    }
}
