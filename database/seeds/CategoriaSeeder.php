<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table("categorias")->insert([
            [
              "id"          => 1,
              'nome'        => 'Bar'
            ],
            [
              "id"          => 2,
              'nome'        => 'Restaurante'
            ],
            [
              "id"          => 3,
              'nome'        => 'Mercado'
            ],
            [
              "id"          => 4,
              'nome'        => 'Mecânica'
            ],
            [
              "id"          => 5,
              'nome'        => 'Pizzaria'
            ],
            [
              "id"          => 6,
              'nome'        => 'Xis'
            ],
            [
              "id"          => 7,
              'nome'        => 'Barbearia'
            ],
            [
              "id"          => 8,
              'nome'        => 'Madeireira'
            ],
            [
              "id"          => 9,
              'nome'        => 'Hotel'
            ],
            [
              "id"          => 10,
              'nome'        => 'Estacionamento'
            ],
            [
              "id"          => 11,
              'nome'        => 'Shopping'
            ],
            [
              "id"          => 12,
              'nome'        => 'Academia'
            ]
          ]);
    }
}
