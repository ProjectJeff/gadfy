<?php

use Illuminate\Database\Seeder;

class PlanoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planos')->insert([
    			'plano' 	=> 'small',
    			'cupons'	=> 5,
    			'valor'		=> '5.99'
    	]);
    	DB::table('planos')->insert([
    			'plano' 	=> 'medium',
    			'cupons'	=> 10,
    			'valor'		=> '10.00'
    	]);
    	DB::table('planos')->insert([
    			'plano' 	=> 'Large',
    			'cupons'	=> 15,
    			'valor'		=> '14.00'
    	]);
    }
}
